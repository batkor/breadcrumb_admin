<?php

namespace Drupal\breadcrumb_admin;

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductType;
use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Provides a breadcrumb builder for articles.
 */
class BreadcrumbAdmin implements BreadcrumbBuilderInterface {

  use StringTranslationTrait;

  /**
   * @var RouteMatchInterface
   */
  protected $route_match;

  protected $used_entity_ids = ['node', 'taxonomy_term', 'commerce_product'];

  protected $current_entity;

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $is_admin = $route_match
      ->getRouteObject()
      ->getOption('_admin_route');
    foreach ($this->used_entity_ids as $used_entity_id) {
      $used = (bool) $route_match->getParameter($used_entity_id);
      if ($is_admin && $used) {
        $this->setCurrentEntity($used_entity_id);
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $breadcrumb->addCacheContexts(['url.path.parent', 'url.path.is_front']);
    
    $links[] = Link::createFromRoute($this->t('Home'), '<front>');

    $entity = $this->getObject($route_match);
    if ($entity) {
      $bundle = $this->getBundle($entity);
      if ($bundle) {
        $links[] = $bundle->toLink($bundle->label(), 'edit-form');
      }
      $links[] = $entity->toLink($entity->label());
    }

    $breadcrumb->setLinks($links);
    return $breadcrumb;
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\Core\Entity\EntityTypeInterface|\Drupal\taxonomy\Entity\Vocabulary|null
   */
  protected function getBundle(EntityInterface $entity) {
    $bundle = NULL;
    if ($entity instanceof Term) {
      $bundle = Vocabulary::load($entity->bundle());
    }

    if ($entity instanceof Node) {
      $bundle = NodeType::load($entity->bundle());
    }

    if ($entity instanceof Product) {
      $bundle = ProductType::load($entity->bundle());
    }
    return $bundle;
  }

  /**
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   * @return mixed|null
   */
  protected function getObject(RouteMatchInterface $route_match) {
    return $route_match->getParameter($this->current_entity);
  }

  protected function setCurrentEntity($entity_id) {
    $this->current_entity = $entity_id;
  }

}
